import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { saveColor } from "../PageReducer/pageAction";
import "../PageThree/pageThree.css";

var arrayStore = [];
class PageThree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colorOne: null,
      colorTwo: null,
      colorThree: null,
      colorFour: null,
      arrayStore: []
    };
  }

  show = events => {
    var data = this.state.arrayStore.includes(events.target.value);
    var index = this.state.arrayStore.indexOf(events.target.value);
    if (data !== false && index > -1) {
      this.state.arrayStore.splice(index, 1);
    } else {
      this.state.arrayStore.push(events.target.value);
    }
  };

  render() {
    this.props.saveColor(this.state.arrayStore);
    return (
      <div>
        <div className="pageIcon">
          <img
            className="pageImage"
            src="../trivia_icon.jpg"
            alt="icon-image"
          />
        </div>
        <div className="firstPage">
          <label>What are the colors in the national flag?</label>
          <form>
            <div>
              <input
                type="checkbox"
                name={this.state.colorOne}
                onClick={this.show}
                value="White"
              />
              White
            </div>
            <div>
              <input
                type="checkbox"
                name={this.state.colorTwo}
                onClick={this.show}
                value="yellow"
              />
              Yellow
            </div>
            <div>
              <input
                type="checkbox"
                name={this.state.colorThree}
                onClick={this.show}
                value="Orange"
              />
              Orange
            </div>
            <div>
              <input
                type="checkbox"
                name={this.state.colorFour}
                onClick={this.show}
                value="Green"
              />
              Green
            </div>
          </form>
          <Link to={{ pathname: "/Summary" }}>
            <input className="submitButton" type="submit" value="submit" />
          </Link>
        </div>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    saveColor: (...data) => dispatch(saveColor(...data))
  };
};

const stateToProps = state => {
  return {};
};

export default connect(stateToProps, dispachToProps)(PageThree);
