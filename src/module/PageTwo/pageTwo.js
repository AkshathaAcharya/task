import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { savePlayer } from "../PageReducer/pageAction";
import "../PageTwo/pageTwo.css";

class PageTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOne: null
    };
  }
  show = events => {
    this.setState({
      selectedOne: events && events.target && events.target.value
    });
  };
  render() {
    this.props.savePlayer(this.state.selectedOne);
    return (
      <div>
        <div className="pageIcon">
          <img
            className="pageImage"
            src="../trivia_icon.jpg"
            alt="icon-image"
          />
        </div>
        <div className="firstPage">
          <label>Who is the best cricketer in the world?</label>
          <form>
            <div>
              <input
                onClick={this.show}
                type="Radio"
                name="name"
                value="Sachin Tendulkar"
              />
              Sachin Tendulkar
            </div>
            <div>
              <input
                onClick={this.show}
                type="Radio"
                name="name"
                value="Virat Kolli"
              />
              Virat Kolli
            </div>
            <div>
              <input
                onClick={this.show}
                type="Radio"
                name="name"
                value="Adam Gilchirst"
              />
              Adam Gilchirst
            </div>
            <div>
              <input
                onClick={this.show}
                type="Radio"
                name="name"
                value="Jacques Kallis"
              />
              Jacques Kallis
            </div>
          </form>
          <Link to={{ pathname: "/pageThree" }}>
            <input className="submitButton" type="submit" value="submit" />
          </Link>
        </div>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    savePlayer: (...data) => dispatch(savePlayer(...data))
  };
};

const stateToProps = state => {
  return {};
};

export default connect(stateToProps, dispachToProps)(PageTwo);
