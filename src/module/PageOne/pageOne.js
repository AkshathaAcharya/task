import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { saveName } from "../PageReducer/pageAction";
import "./pageOne.css";

class PageOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null
    };
  }
  methodName = events => {
    this.setState({
      name: events && events.target && events.target.value
    });
  };
  render() {
    this.props.saveName(this.state.name);
    return (
      <div>
        <div className="pageIcon">
          <img
            className="pageImage"
            src="../trivia_icon.jpg"
            alt="icon-image"
          />
        </div>
        <div className="firstPage">
          <label>What is your name?</label>
          <input
            type="text"
            name="name"
            className="textBox"
            placeholder="What is your name"
            onChange={this.methodName}
            value={this.state.name}
            required
          />
          <Link to={{ pathname: "/pageTwo" }}>
            <input className="submitButton" type="submit" value="submit" />
          </Link>
        </div>
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    saveName: (...data) => dispatch(saveName(...data))
  };
};

const stateToProps = state => {
  return {};
};

export default connect(stateToProps, dispachToProps)(PageOne);
