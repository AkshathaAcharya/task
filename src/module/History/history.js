import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "../History/history.css";

class History extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log(this.props.historyData);
    return (
      <div>
        <div className="pageIcon">
          <img
            className="pageImage"
            src="../trivia_icon.jpg"
            alt="icon-image"
          />
        </div>
        {this.props.historyData !== undefined && (
          <div className="firstPage">
            <h1>History</h1>
            <p>
              {this.props.historyData.map((item, index) => {
                var count = index + 1;
                return (
                  <div key={index}>
                    <h4>Game{count}: </h4>
                    <p>Date- {item.day}</p>
                    <p>Name:{item.data.name}</p>
                    <p>Who is the best cricketer in the world? </p>
                    <p>{item.data.player}</p>
                    <p> What are the colors in the national flag?</p>
                    <p>
                      <i>
                        {item.data.color !== undefined &&
                          item.data.color.map((colorItem, colorIndex) => {
                            return <li key={colorIndex}>{colorItem}</li>;
                          })}
                      </i>
                    </p>
                    <hr />
                  </div>
                );
              })}
            </p>
            <div>
              <span>
                <Link to={{ pathname: "/pageOne" }}>
                  <input
                    className="submitButton"
                    type="submit"
                    value="Finish"
                  />
                </Link>
              </span>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {};
};

const stateToProps = state => {
  return {
    historyData: state.PageOneReducer.historyData
  };
};

export default connect(stateToProps, dispachToProps)(History);
