import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";

import PageOne from "../../module/PageOne/pageOne";
import PageTwo from "../../module/PageTwo/pageTwo";
import PageThree from "../../module/PageThree/pageThree";
import Summary from "../../module/Summary/summary";
import History from "../../module/History/history";

const splash = document.querySelector(".splash");
document.addEventListener("DOMContentLoaded", e => {
  setTimeout(() => {
    splash.classList.add("display-none");
  }, 2000);
});
class App extends React.Component {
  componentDidMount() {}
  render() {
    return (
      <div>
        <div>
          <Router>
            <Switch>
              <div>
                <Route exact path="/pageOne" component={PageOne} />
                <Route exact path="/pageTwo" component={PageTwo} />
                <Route exact path="/pageThree" component={PageThree} />
                <Route exact path="/summary" component={Summary} />
                <Route exact path="/history" component={History} />
                <Redirect to="/pageOne" />
              </div>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
