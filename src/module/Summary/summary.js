import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { addHistory } from "../PageReducer/pageAction";
import "../Summary/summary.css";

class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  loop = () => {
    this.props.addHistory(this.props.summary);
  };
  render() {
    return (
      <div>
        <div className="pageIcon">
          <img
            className="pageImage"
            src="../trivia_icon.jpg"
            alt="icon-image"
          />
        </div>
        {this.props.summary !== undefined && (
          <div className="firstPage">
            <h1>Summary</h1>
            <p>Hello {this.props.summary.name}</p>
            <p>Here are the answers selected: </p>
            <p>
              <b>Who is the best cricketer in the world?</b>
            </p>
            <p>
              <i>{this.props.summary.player}</i>
            </p>
            <p>
              <b>What are the colors in the national flag?</b>
            </p>
            <p>
              <i>
                {this.props.summary.color !== undefined &&
                  this.props.summary.color.map((item, index) => {
                    return <li key={index}>{item}</li>;
                  })}
              </i>
            </p>
            <div>
              <span>
                <Link to={{ pathname: "/History" }}>
                  <input
                    className="submitButton"
                    type="submit"
                    value="History"
                    onClick={this.loop}
                  />
                </Link>
              </span>
              <span>
                <Link to={{ pathname: "/pageOne" }}>
                  <input
                    onClick={this.loop}
                    y
                    className="submitButton"
                    type="submit"
                    value="Finish"
                  />
                </Link>
              </span>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const dispachToProps = dispatch => {
  return {
    addHistory: (...data) => dispatch(addHistory(...data))
  };
};

const stateToProps = state => {
  return {
    summary: state.PageOneReducer.summary
  };
};

export default connect(stateToProps, dispachToProps)(Summary);
