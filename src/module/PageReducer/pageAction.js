const SAVE_NAME = "SAVE_NAME";
const SAVE_PLAYER = "SAVE_PLAYER";
const SAVE_COLOR = "SAVE_COLOR";
const ADD_HISTORY = "ADD_HISTORY";

export const saveName = data => {
  return {
    type: SAVE_NAME,
    payload: data
  };
};

export const savePlayer = data => {
  return {
    type: SAVE_PLAYER,
    payload: data
  };
};
export const saveColor = data => {
  return {
    type: SAVE_COLOR,
    payload: data
  };
};
export const addHistory = data => {
  var date = new Date();
  var day = date.toLocaleString();
  // console.log(a);
  // var day = date.getMonth();
  // var year = date.getFullYear;
  // var time = date.getHours();

  return {
    type: ADD_HISTORY,
    payload: { data: data, day: day }
  };
};
