import getReducerFromObject from "../../Utlis/reducer.file";
const newItem = [];
const initialState = {
  username: "",
  player: "",
  color: "",
  summary: [],
  historyData: []
};

const PageOneReducer = getReducerFromObject([], {
  SAVE_NAME: (state, action) => {
    return {
      username: action.payload,
      player: state.player,
      color: state.color,
      summary: state.summary,
      historyData: { ...state.historyData }
    };
  },

  SAVE_PLAYER: (state, action) => {
    return {
      username: state.username,
      player: action.payload,
      color: state.color,
      summary: state.summary,
      historyData: { ...state.historyData }
    };
  },
  SAVE_COLOR: (state, action) => {
    return {
      username: state.username,
      player: state.player,
      color: action.payload,
      summary: {
        ...state.summary,
        name: state.username,
        player: state.player,
        color: action.payload
      },
      historyData: { ...state.historyData }
    };
  },
  ADD_HISTORY: (state, action) => {
    newItem.push(action.payload);
    return {
      historyData: newItem,
      username: "",
      player: "",
      color: "",
      summary: ""
    };
  }
});

export default PageOneReducer;
