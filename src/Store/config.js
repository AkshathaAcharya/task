import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { rootsaga } from "./root.saga";
import Reducer from "./root.reducer";
import { composeWithDevTools } from "redux-devtools-extension";

const sagaMiddleware = createSagaMiddleware();
const Store = createStore(
  Reducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);
sagaMiddleware.run(rootsaga);

export default Store;
