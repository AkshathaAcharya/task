import { combineReducers } from "redux";
import PageOneReducer from "../module/PageReducer/pageReducer";

const Reducer = combineReducers({
  PageOneReducer
});

export default Reducer;
